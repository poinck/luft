#!/usr/bin/Rscript

library(xml2)


# load configuration
rc <- read.csv(file = "~/.luftrc", sep = "=", header = FALSE,
    col.names = c("key", "value"))
#str(rc)
i <- 1
found_k <- 1
rh_offset <- 0
for (k in rc$key) {
    if (k == "luft_path") {
        found_k <- i
    } else if (k == "rh_offset") {
        rh_offset <- as.numeric(as.character(rc$value[i]))
    }
    #cat("k =", k, ", v =", rc$value, "\n")
    i <- i + 1
}
#cat("rh_offset:", rh_offset, "\n")
feinstaub_path <- "./"
i <- 1
for (v in rc$value) {
    if (i == found_k) {
        feinstaub_path <- v
    }
    i <- i + 1
}

mm <- read_xml(paste0(feinstaub_path, "station.kml"))

time <- xml_find_all(mm, "//dwd:TimeStep")
times <- as.POSIXct(xml_text(xml_contents(time)),
    format = "%Y-%m-%dT%H:%M", tz = "UTC")
times <- times[1:96]
#str(times)

value <- xml_find_all(mm, "//dwd:value")
values <- xml_text(xml_contents(value))

pppp <- as.numeric(unlist(strsplit(values[1], "\\s+"))) / 100
pppp <- pppp[2:97]
#str(pppp)

ttt <- as.numeric(unlist(strsplit(values[4],  "\\s+"))) - 273.15
ttt <- ttt[2:97]
#str(ttt)

#td <- as.numeric(unlist(strsplit(values[6], c("     ")))) - 273.15
td <- as.numeric(unlist(strsplit(values[6], "\\s+"))) - 273.15
td <- td[2:97]
#str(td)

# wind speed
ff <- as.numeric(unlist(strsplit(values[14], "\\s+")))
ff <- ff[2:97]
#str(ff)

# wind gust
fx1 <- as.numeric(unlist(strsplit(values[16], "\\s+")))
fx1 <- fx1[2:97]

# cloud cover
#neff <- as.numeric(unlist(strsplit(values[26], "\\s+")))
#neff <- neff[2:97]
#str(neff)

# low clouds
nl <- as.numeric(unlist(strsplit(values[30], "\\s+")))
nl <- nl[2:97]

# high clouds
nh <- as.numeric(unlist(strsplit(values[28], "\\s+")))
nh <- nh[2:97]

# mid clouds
nm <- as.numeric(unlist(strsplit(values[29], "\\s+")))
nm <- nm[2:97]

# thunder
wwt <- as.numeric(unlist(strsplit(values[54], "\\s+")))
wwt <- wwt[2:97]
wwt <- wwt * .01
#print(wwt)

# total precipitation consistent with significant weather (kg/m2, rain or snow)
tprc <- as.numeric(unlist(strsplit(values[71], "\\s+")))
tprc <- tprc[2:97]

# propability of percipitation
pp <- as.numeric(unlist(strsplit(values[41], "\\s+")))
pp <- pp[2:97]
#str(rr1c)
#rr1c[rr1c < 34] <- 0
pp <- pp * .01
#print(pp)
#print(values)

# global irradiance (kJ/m2)
rad1h <- as.numeric(unlist(strsplit(values[106], "\\s+")))
rad1h <- rad1h[2:97]
# to W/m2
rad1h <- rad1h / 3.6
#str(rad1h)

rh <- 100 * (exp((17.625 * td)/(243.04 + td)) / exp((17.625 * ttt) / (243.04 + ttt)))

f <- data.frame(
    time = times,
    tmp = ttt,
    dew = td,
    hpa = pppp,
    #tcc = neff,
    ccl = nl,
    cch = nh,
    ccm = nm,
    swr = rad1h,
    win = ff,
    gust = fx1,
    tprc = tprc,
    pp = pp,
    hum = rh,
    thunder = wwt
)
#str(f)
save(f, file = paste0(feinstaub_path, "forecast.rda"))
cat("forecast saved\n")
