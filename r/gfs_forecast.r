#!/usr/bin/Rscript

require(rNOMADS, quietly = TRUE)

last_run <- 1

# load configuration
rc <- read.csv(file = "~/.luftrc", sep = "=", header = FALSE,
    col.names = c("key", "value"))
#str(rc)
#print(rc)
i <- 1
found_k <- 1
rh_offset <- 0
for (k in rc$key) {
    if (k == "luft_path") {
        found_k <- i
    } else if (k == "rh_offset") {
        rh_offset <- as.numeric(as.character(rc$value[i]))
    }
    #cat("k =", k, ", v =", rc$value, "\n")
    i <- i + 1
}
#cat("rh_offset:", rh_offset, "\n")
feinstaub_path <- "./"
i <- 1
for (v in rc$value) {
    if (i == found_k) {
        feinstaub_path <- v
    }
    i <- i + 1
}

urls <- CrawlModels(abbrev = "gfs_1p00", depth = last_run)
print(urls)
    # it is always good to use a model run that is 6 hours old, otherwise
    # downloads may fail
url <- paste(urls[last_run],
    "&subregion=&leftlon=14&rightlon=14&toplat=52&bottomlat=52",
    sep = "")
parameters <- ParseModelPage(url)
str(parameters)
print(parameters$variables)
forecasts <- parameters$pred[2:32] # approx 4 days
str(forecasts)
#variables <- c("TMP", "RH", "PRMSL", "")
variables = c("TMP", "RH", "DPT", "PRMSL", "TCDC", "PRATE", "UGRD", "VGRD")
            #  tmp    hum   dew    hpa      tcc     tpr
levels <- c("2 m above ground", "mean sea level", "surface",
    "entire atmosphere", "10 m above ground")

cat("grab grib (model info):\n")
model.info <- GribGrab(url, forecasts, levels, variables,
    local.dir = feinstaub_path)
    # FIXME: only grab files if previous are older than 5-6 hours on disk

grb_filenames <- paste0(feinstaub_path, forecasts, ".grb")
print(grb_filenames)
gribfile_list <- ""
for (grb_filename in grb_filenames) {
    gribfile_list <- paste(gribfile_list, grb_filename, sep = " ")
}
shell_cmd <- paste0("cat ", gribfile_list, " > ",
    feinstaub_path, "fall.grb")
cat("shell_cmd =", shell_cmd, "\n")
try(system(shell_cmd))

model <- ReadGrib(paste0(feinstaub_path, "fall.grb"), levels, variables)
str(model)
#print(model$forecast.date)
#print(model$variables)
#print(model$value)
#print(model$levels)

# extract values from grib
#print(model$forecast.date)
#print(model$value[model$variables == "UGRD"]) # u = zonal/east->west
#print(model$value[model$variables == "VGRD"]) # v = meridional/north->south
#print(w)
f <- data.frame(
    tcc = c(model$value[model$variables == "TCDC"], NA),
    dew = model$value[model$variables == "DPT"] - 273.15
)
#str(f)
f$tmp = model$value[model$variables == "TMP" & model$levels == "2 m above ground"] - 273.15
f$hum = model$value[model$variables == "RH"]
f$hpa = model$value[model$variables == "PRMSL"] * .01
f$run <- as.POSIXct(model$model.run.date[1:length(f$time)], tz = "UTC")
f$time = seq(as.POSIXct(model$forecast.date[1], tz = "UTC"),
    as.POSIXct(model$forecast.date[length(model$forecast.date)], tz = "UTC"),
    by = "3 hour")

tpr_time_aggr <- cut(x = as.POSIXct(model$forecast.date[model$variables == "PRATE"]),
    breaks = f$time)
tpr_aggr <- aggregate(model$value[model$variables == "PRATE"], FUN = sum,
    by = list(time_aggr = tpr_time_aggr), na.rm = TRUE)
tpr_aggr$x <- tpr_aggr$x * 60 * 60
f$tpr <- c(tpr_aggr$x, NA)
#print(tpr_aggr)
f$win <- sqrt((model$value[model$variables == "UGRD"])**2 + (model$value[model$variables == "VGRD"])**2)

str(f)

# save to rda
save(f, file = paste0(feinstaub_path, "forecast.rda"))

