#!/usr/bin/Rscript

library(rNOMADS)

# load configuration
rc <- read.csv(file = "~/.luftrc", sep = "=", header = FALSE,
    col.names = c("key", "value"))
#str(rc)
#print(rc)
i <- 1
found_k <- 1
rh_offset <- 0
for (k in rc$key) {
    if (k == "luft_path") {
        found_k <- i
    } else if (k == "rh_offset") {
        rh_offset <- as.numeric(as.character(rc$value[i]))
    }
    #cat("k =", k, ", v =", rc$value, "\n")
    i <- i + 1
}
#cat("rh_offset:", rh_offset, "\n")
feinstaub_path <- "./"
i <- 1
for (v in rc$value) {
    if (i == found_k) {
        feinstaub_path <- v
    }
    i <- i + 1
}

grib_file <- paste0(feinstaub_path, "homecosmo.grib2")

variables = c("NSWRF",
    "TMP", "RH", "DPT", "PRMSL", "TCDC", "TPRATE", "UGRD", "VGRD")
    #  tmp    hum   dew    hpa      tcc     tpr
levels = c("surface",
    "2 m above ground", "mean sea level", "surface", "10 m above ground")
model <- ReadGrib(grib_file, levels, variables)
str(model)
print(model$forecast.date)
#print(model$levels)
#print(model$value[model$variables == "TPRATE"])

swrs <- model$value[model$variables == "NSWRF"]
swrs_orig <- swrs
cat("cumulative average SWR:\n")
print(swrs)
for(i_swr in 1:length(swrs)) {
    if (i_swr > 2) {
        swrs[i_swr] <- i_swr * swrs[i_swr] - sum(swrs[1:i_swr-1])
    }
}
cat("hourly SWR:\n")
print(swrs)
f_dwd <- data.frame(
    time = seq(as.POSIXct(model$forecast.date[1], tz = "UTC"),
        as.POSIXct(model$forecast.date[length(model$forecast.date)], tz = "UTC"),
        by = "1 hour"),
    tmp = model$value[model$variables == "TMP"] - 273.15,
    hum = model$value[model$variables == "RH"],
    dew = model$value[model$variables == "DPT"] - 273.15,
    hpa = model$value[model$variables == "PRMSL"] * .01,
    tcc = model$value[model$variables == "TCDC"],
    swr = swrs
)
f_dwd$run <- as.POSIXct(model$model.run.date[1:length(f_dwd$time)], tz = "UTC")

# calculate percipitation per hour
l <- length(model$value[model$variables == "TPRATE"])
tpr <- rep(NA, l)
tpr[1] <- model$value[model$variables == "TPRATE"][1]
for (i in c(2:l)) {
    tpr[i] <- model$value[model$variables == "TPRATE"][i] - model$value[model$variables == "TPRATE"][i-1]
}
#print(tpr)
tpr_time_aggr <- cut(x = as.POSIXct(model$forecast.date[model$variables == "TPRATE"]), breaks = f_dwd$time)
#cat("lengths:", length(tpr), length(tpr_time_aggr), "\n")
tpr_aggr <- aggregate(tpr, FUN = sum,
    by = list(time_aggr = tpr_time_aggr), na.rm = TRUE)
f_dwd$tpr <- rep(NA, length(f_dwd$time))
#print(length(f_dwd$tpr))
f_dwd$tpr <- replace(x = f_dwd$tpr, list = c(1:length(f_dwd$tpr)),
    values = tpr_aggr$x)
#print(f_dwd$tpr)

# wind
f_dwd$win <- sqrt((model$value[model$variables == "UGRD"])**2 + (model$value[model$variables == "VGRD"])**2)
#print(f_dwd$win)

str(f_dwd)
#print(f_dwd$time)

# save to rda
save(f_dwd, file = paste0(feinstaub_path, "dwd_forecast.rda"))
