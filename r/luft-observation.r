#!/usr/bin/Rscript

# load configuration
rc <- read.csv(file = "~/.luftrc", sep = "=", header = FALSE,
    col.names = c("key", "value"))
#str(rc)
i <- 1
found_k <- 1
rh_offset <- 0
for (k in rc$key) {
    if (k == "luft_path") {
        found_k <- i
    } else if (k == "rh_offset") {
        rh_offset <- as.numeric(as.character(rc$value[i]))
    }
    #cat("k =", k, ", v =", rc$value, "\n")
    i <- i + 1
}
#cat("rh_offset:", rh_offset, "\n")
feinstaub_path <- "./"
i <- 1
for (v in rc$value) {
    if (i == found_k) {
        feinstaub_path <- v
    }
    i <- i + 1
}

dahlem <- read.csv(paste0(feinstaub_path, "dahlem.txt"), sep = ";")
#str(dahlem)
# WW (glatteis): 24 (gering), 56 (mittel), 57 (stark), 66 (mittel), 67 (stark)
times <- as.POSIXct(as.character(dahlem$MESS_DATUM),
    format = "%Y%m%d%H", tz = "Europe/Berlin")
#times <- times[1:96]
print(times[length(times)])

o <- data.frame(
    gle = glatteis
)
#str(o)
save(o, file = paste0(feinstaub_path, "observation.rda"))
cat("observation saved\n")
